<?php
/**
 *  @Entity @Table(name="slam3.instrument")
 */

class Instrument{
    
    /**
* @Id @Column(type="string", length=5)
**/
    private $ref;
    
    /**
* @Column(type="string", length=30)
**/
    private $nom;
    
        /**
* @Column(type="string", length=20)
**/
    private $marque;
    
        /**
* @Column(type="string", length=150)
**/
    private $caracteristique;
    
        /**
* @Column(type="integer")
**/
    private $prix;
    
        /**
* @Column(type="string", length=20)
**/
    private $photo;

/**
* @ManyToOne(targetEntity="Categorie")
* @JoinColumn (name="categorie", referencedColumnName="code_categorie")
**/
    private $categorie;
    
    public function __Instrument()
    {
        $this->ref = 1;
        $this->marque = "";
        $this->caracteristique = "";
        $this->prix = 0;
        $this->nom = "";
        $this->photo = "";
        $this->categorie = new Categorie(); 
    }
    
    // constructeur avec paramètres pour initialiser les propriétés de Region
public function init($ref, $nom, $marque, $caracteristique, $photo, $categorie)
{
$this->ref = 1;
        $this->marque = $marque;
        $this->caracteristique = $caracteristique;
        $this->prix = $ref;
        $this->nom = $nom;
        $this->photo = $photo;
        $this->categorie = $categorie; 
}

function getRef() {
    return $this->ref;
}

function getNom() {
    return $this->nom;
}

function getMarque() {
    return $this->marque;
}

function getCaracteristique() {
    return $this->caracteristique;
}

function getPrix() {
    return $this->prix;
}

function getPhoto() {
    return $this->photo;
}

function setRef($ref) {
    $this->ref = $ref;
}

function setNom($nom) {
    $this->nom = $nom;
}

function setMarque($marque) {
    $this->marque = $marque;
}

function setCaracteristique($caracteristique) {
    $this->caracteristique = $caracteristique;
}

function setPrix($prix) {
    $this->prix = $prix;
}

function setPhoto($photo) {
    $this->photo = $photo;
}

public function getCategorie() {
    return $this->categorie;
}

function setCategorie($categorie) {
    $this->categorie = $categorie;
}



}
?>
