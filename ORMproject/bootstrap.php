<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

require_once 'vendor/autoload.php';

$config = Setup::createAnnotationMetadataConfiguration(array(__DIR__."/src"),true);

$connexion = array (
    'driver' => 'pdo_pgsql',
    'host' => 'postgresql.bts-malraux72.net',
    'port' => '5432',
    'dbname' => 't.lehugeur',
    'user' => 't.lehugeur',
    'password' => 'P@ssword',
);

$entityManager = EntityManager::create($connexion, $config);
?>
